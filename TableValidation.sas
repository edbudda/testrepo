options mprint mlogic;

%macro fileunzip2(oldtable=,
                 newpath=)
                 ;

data _null_;
call system ("cd &newpath");
call system ("7za e &oldtable..7z");
%let cycles = 0;
%processwait(filecheck=&newpath.&_sl.&oldtable);

run;

%mend fileunzip2;

%macro extract(tablein=);

%copysource(newpath=/hipaa/0211942.200_MIDS_PGM/Readmissions/pgm/ebudda/snfrm/data/temp,
			oldpath=/hipaa/0211942.200_MIDS_PGM/Readmissions/pgm/data/bbaker/yr2018_rmfy/yr2018rmfy_q2,
			oldtable=&tablein,
			sourcetype=7z)


%fileunzip2(oldtable=&tablein,
            newpath=/hipaa/0211942.200_MIDS_PGM/Readmissions/pgm/ebudda/snfrm/data/temp)

filename in1 "/hipaa/0211942.200_MIDS_PGM/Readmissions/pgm/ebudda/snfrm/data/temp/&tablein.";

%let tablein1=%scan(&tablein,-2,'.');

%put &=tablein1;

proc cimport infile=in1 data=temp.&tablein1.;

%mend extract;

/*%extract(tablein=ti00.@nhrm18a.ti02.xportm.medp2018.trn)*/
/*%extract(tablein=ti00.@nhrm18a.ti02.xportm.medp2017.trn)*/
/*%extract(tablein=ti00.@nhrm18a.ti02.xportm.medp2016.trn)*/
/*%extract(tablein=ti00.@nhrm18a.ti02.xportd.den2018.trn)*/
/*%extract(tablein=ti00.@nhrm18a.ti02.xportd.den2017.trn)*/
/*%extract(tablein=ti00.@nhrm18a.ti02.xportd.den2016.trn)*/
/*%extract(tablein=ti00.@nhrm18a.ti02.xportx.xref2018.trn)*/

data temp.den(drop=file_yr);
set temp.den2018 (keep=hicno file_yr)
    temp.den2017 (keep=hicno file_yr)
    temp.den2016 (keep=hicno file_yr)
;
file_yr_den = file_yr;
run;

data temp.medp;
set temp.medp2018 (in=a keep=hicno admsn_dt)
    temp.medp2017 (in=b keep=hicno admsn_dt)
    temp.medp2016 (in=c keep=hicno admsn_dt)
;
if a then file_yr_medp='2018';
if b then file_yr_medp='2017';
if c then file_yr_medp='2016'; 
admsn_dt_YYMM = put(admsn_dt,yymm5.);
run;

proc sort nodupkey data=temp.den;
by hicno;

proc sort data=temp.medp;
by hicno;

run;

data temp.merge (keep=in_den in_medp file_yr_den file_yr_medp admsn_dt_YYMM);
merge temp.den (in=a)
      temp.medp (in=b);
by hicno;
in_den=a;
in_medp=b;

run; 

proc summary data=temp.merge missing print;
where in_den = 0;
class in_den in_medp file_yr_den file_yr_medp admsn_dt_YYMM;

run;

